Issue Fixes in Foxit Reader 5.4.3
======================================================================================================================

Fixed a security issue where the insecure application loading libraries could be exploited to attack the application.




What's New in Foxit Reader 5.4
======================================================================================================================
New Features:

1.Supports opening PDF files on Microsoft SharePoint Server and allowing users to seamlessly check out and check in PDF files within Foxit Reader.

2.Allows users to easily send, sign and save PDF documents and forms using DocuSign, the industry's most widely used eSignature solution.

Issue Fixes:

1.Fixed an issue where Foxit Reader may call and run malicious code in the Dynamic Link Library (DLL) file.

2.Fixed an issue where the application cannot remember the last print paper size setting.

3.Fixed an issue where certain digital signatures cannot be verified by Foxit Reader. 




Issue Fixes in Foxit Reader 5.3.1
======================================================================================================================

1.Fixed an issue where some special PDFs could not be opened because of a problem with the ��loading font�� process.

2.Fixed an issue with using Hotmail account after installing Foxit Reader.

3.Fixed an issue where the Foxit Reader plug-in cannot be loaded in Opera and will cause that other plug-ins cannot be loaded as well.

4.Fixed an issue caused by using track point to scroll PDFs on Thinkpad.

5.Fixed an issue where the cursor cannot be moved to the beginning of PDF content with the short-cut key ��Ctrl + Left��.




What's New in Foxit Reader 5.3
======================================================================================================================
New Features:

1.Supports viewing RMS-protected PDF files with the dynamic watermark.

2.Added the option ��Tile Large Pages�� to the print dialog box. With this new feature, users can tile pages that are larger than the selected paper size at a specified scale. The whole page content will be divided and printed on pieces of paper.

3.Provides users with three update options, allowing users selecting the most appropriate upgrade method according to their requirements.

4.Automatically changes the hand tool to the select text tool after moving the cursor to the text content. 

5.Supports controlling preference dialogue with the keyboard shortcuts.

Improvements:

1.Added the option ��Read from Current Page�� to the Read Out Loud feature, which makes the system read out the PDF content from current page to the end of the file.

2.Improved the user interface elements, including arrow comment, Facebook interface, etc.

3.Added the toolbar mode switch icon to the toolbar, simplifying user��s manipulation.

4.Shows the progress bar before displaying the content when opening the PDFs in the web browser.

5.Improved the accuracy of highlight location, enabling the highlight area more agree with the target text.

6.In the Full Screen mode, places the toolbar in the location adjusted last time.

7.Supports using arrowheads to switch the file tabs when opening multiple PDF files, which is convenient for users when the tabs cannot be arranged in a row. 


Issue Fixes:

1.Fixed an issue where users cannot open the attachments of PDF files in XP and Windows7.

2.Fixed an issue where the high screen resolution disordered the layout of the installation interface.

3.Fixed an issue where note comments cannot move with the marked content when dragging the scroll bar to view a file.

4.Fixed an issue where a large blank area appeared in the toolbar when reading PDF in web browser.

5.Fixed an issue where users cannot use the arrow keys to view the search results.

6.Fixed an issue where the system executed an action if user typed the shortcut key in the text input box.

7.Fixed an issue where users can only selected the Snapshot area in the current visible area.




Issue Fixes in Foxit Reader 5.1.4.0104
======================================================================================================================

Fixed an intermittent issue where tool bar and menu bar are not displayed properly. 



Issue Fixes in Foxit Reader 5.1.3.1201
======================================================================================================================

1.Fixed an unexpected termination issue of Foxit Reader opened inside a web browser when switching interface languages. 

2.Fixed an issue where paper size in the Preview Area of Print Dialogue Box cannot be updated accordingly when using Xerox Printers.

3.Fixed an unexpected termination issue of Foxit Reader when switching the interface language.

4.Fixed an issue where the Paper Drawer cannot be changed to Cassette when printing.

5.Fixed an unexpected termination issue of Foxit Reader when opening certain PDF files.




What's New in Foxit Reader 5.1
======================================================================================================================
New Features:

1.For better reading experience with maximized  PDF document display area, Foxit Reader supports showing PDFs in Reading Mode that hides the toolbar, navigation pane and status bar, with only the menu bar available.

2.In Tabbed Toolbar Mode, Foxit Reader supports collapsing/expanding the toolbar area to maximize the reading area.

3.To extend the document pane, the Auto-hide Status Bar feature hides the Status Bar during reading and shows the status bar floating at the bottom of the page only when the cursor is moved to the bottom.

4.Provides integration to the social networking sites to satisfy Facebook and Twitter users.

5.Supports Read out loud function.

6.Supports displaying PDF documents in Reading mode or Full Screen Mode automatically when opening PDFs with Foxit Reader.

7.Foxit Reader enhances the text rendering quality to display the text more clearly and neat.

8.Increased performance for reader startup, opening a PDF file, and closing a PDF file. 

9.Automatically chooses paper type according to the page size of PDFs when printing.

10.Show the page thumbnails when dragging the scrollbar in a single page display mode.

11.Added the Foxit Reader classic skin.

12.Disabled spell check function in the Typewriter/Form Mode as the default option of the preference.

13.Improved the accuracy of spell check.

14.Moved the comment and attachment pane to be aligned with bookmark, thumbnail, layers and signatures panes.

15.Fixed many bugs and enhanced the performance.




Issue Fixes in Foxit Reader 5.0.2.0718
======================================================================================================================

1.Fixed a security issue of arbitrary code execution when opening certain PDF files.

2.Fixed an unexpected termination issue of Foxit Reader when opening certain PDF files in a web browser.

3.Fixed an issue where the page content cannot be displayed when opening certain PDF files in a web browser.

4.Fixed an issue where the desktop icons would be rearranged automatically when creating the desktop icon of Foxit Reader 5.0 during installation on Windows XP. 

5.Fixed an issue where the file name would be a messy code or its extension would be missed when emailing certain PDF files from a web browser.

6.Recovered the Print Scale function which was available in pre 5.0 versions.




What's New in Foxit Reader 5.0
======================================================================================================================
New Features:

1.Supports XFA form filling.

2.Supports to open the RMS�Cprotected PDFs with Foxit Reader. 

3.Supports to customize shortcut keys for various commands.

4.The new Split View mode allows you to divide the PDFs into two panes or four panes.

5.Fit Visible mode enables the PDF pages to fit the width of the window.

6.Supports Tabbed Toolbar Mode.

7.Supports 11 different page transition types in the full screen mode.

8.Offers four nice skins for appearance change.

9.Supports to search keywords in the comment and bookmark.

10.Enables you to preview a PDF attachment in Microsoft Outlook. 

11.Displays the PDF files in the Windows Explorer as the thumbnails if Foxit Reader has been set as the default PDF viewer.

12.Adds more available command lines.


Issue Fixes:

Fixed an unexpected termination of Foxit Reader when opening some affected PDF files.
	



Download the Latest Version
========================================================================================================================
You can download the latest version of Foxit Reader from our website at www.foxitsoftware.com.




Contact Us
========================================================================================================================
If you have any questions using Foxit Reader, please contact us by email:

Sales and Information - sales@foxitsoftware.com
Marketing Serivce - marketing@foxitsoftware.com
Technical Support - support@foxitsoftware.com
Website Questions - webmaster@foxitsoftware.com



Foxit Corporation

Address: 42840 Christy Street. Suite 201, Fremont CA 94538, USA

Sales Phone: 1-866-680-3668 (24/7)

Support Phone: 1-866-MYFOXIT or 1-866-693-6948(24/7)

Fax: 510-405-9288

Web: www.foxitsoftware.com
